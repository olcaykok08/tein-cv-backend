package com.example.demo.user.controller;
import com.example.demo.user.model.UserDTO;
import com.example.demo.user.model.UserEntity;
import com.example.demo.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("public-driverapp-api")

public class UserResource {
    @Autowired
    private UserService userService;

    @PostMapping("/login")
    ResponseEntity<Boolean> login(@RequestBody UserDTO userDTO) {
        return ResponseEntity.ok(userService.login(userDTO));
    }

    @PostMapping("/cv/save")
    ResponseEntity<UserDTO> cvSave(@RequestBody UserDTO userDTO){
        UserDTO result = userService.save(userDTO);
        if (result!=null) {
            return ResponseEntity.ok(result);
        }
        return ResponseEntity.notFound().build();
    }
}
