package com.example.demo.user.model;
import lombok.Data;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
import java.util.Date;
@Entity
@Data
@Table
public class UserEntity {
    @Id
    @Column
    private Long id;
    @Column
    private String email;
    @Column
    private String name;
    @Column
    private String surname;
    @Column
    private String address;
    @Column
    private String phoneNumber;
    @Column
    private String oldCompany;
    @Column
    private String job;
    @Column
    private Date expiredCompanyDate;
    @Column
    private String description;
    @Column
    private String education;
    @Column
    private Long startYear;
    @Column
    private Long finishYear;
}
