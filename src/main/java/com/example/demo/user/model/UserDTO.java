package com.example.demo.user.model;

import lombok.Data;

import java.util.Date;

@Data
public class UserDTO {

    private Long id;
    private String email;
    private String name;
    private String surname;
    private String address;
    private String phoneNumber;
    private String oldCompany;
    private String job;
    private Date expiredCompanyDate;
    private String description;
    private String education;
    private Long startYear;
    private Long finishYear;
    private String role;
}
