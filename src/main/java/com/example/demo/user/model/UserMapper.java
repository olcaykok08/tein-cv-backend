package com.example.demo.user.model;

import com.example.demo.user.model.UserDTO;
import com.example.demo.user.model.UserEntity;

public class UserMapper {

    public static UserDTO toDto(UserEntity user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        userDTO.setSurname(user.getSurname());
        userDTO.setAddress(user.getAddress());
        userDTO.setEducation(user.getEducation());
        userDTO.setDescription(user.getDescription());
        userDTO.setEmail(user.getEmail());
        userDTO.setFinishYear(user.getFinishYear());
        userDTO.setExpiredCompanyDate(user.getExpiredCompanyDate());
        userDTO.setPhoneNumber(user.getPhoneNumber());
        userDTO.setStartYear(user.getStartYear());
        userDTO.setJob(user.getJob());
        userDTO.setOldCompany(user.getOldCompany());
        return userDTO;
    }

    public static UserEntity toEntity(UserDTO userDTO) {
        UserEntity user = new UserEntity();
        user.setId(userDTO.getId());
        user.setName(userDTO.getName());
        user.setSurname(userDTO.getSurname());
        user.setAddress(userDTO.getAddress());
        user.setEducation(userDTO.getEducation());
        user.setDescription(userDTO.getDescription());
        user.setEmail(userDTO.getEmail());
        user.setFinishYear(userDTO.getFinishYear());
        user.setExpiredCompanyDate(userDTO.getExpiredCompanyDate());
        user.setPhoneNumber(userDTO.getPhoneNumber());
        user.setStartYear(userDTO.getStartYear());
        user.setJob(userDTO.getJob());
        user.setOldCompany(userDTO.getOldCompany());
        return user;
    }
}
