package com.example.demo.user.service;
import com.example.demo.user.model.UserDTO;
import com.example.demo.user.model.UserEntity;
import com.example.demo.user.model.UserMapper;
import com.example.demo.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;
    public UserDTO save(UserDTO userDTO) {
         UserEntity user = UserMapper.toEntity(userDTO);
         UserEntity result = userRepository.save(user);
         return UserMapper.toDto(result);
    }
    public Boolean login(UserDTO userDTO) {
        List<UserEntity> user = userRepository.findAll();
        return null;
    }
}